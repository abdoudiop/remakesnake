﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class MovingSnake : MonoBehaviour
{
    Vector3 direction = Vector3.left;

    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("Move", 0.5f, 0.5f);
    }

    // Update is called once per frame
    void Update()
    {
        // Move in a new Direction?
    if (Input.GetKey(KeyCode.RightArrow))
            direction = Vector3.right;
        else if (Input.GetKey(KeyCode.DownArrow))
            direction = -Vector3.forward;    // '-up' means 'down'
        else if (Input.GetKey(KeyCode.LeftArrow))
            direction = Vector3.left; // '-right' means 'left'
        else if (Input.GetKey(KeyCode.UpArrow))
            direction = Vector3.forward;
    }

    void Move()
    {
        // Move head into new direction
        transform.Translate(direction);
    }
}
