﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GenerateFood : MonoBehaviour
{
    public GameObject foodPrefab;

 
    public Transform borderTop;
    public Transform borderBottom;
    public Transform borderLeft;
    public Transform borderRight;


    public Material redMaterial;
    public Material greenMaterial;
    public Material yellowMaterial;
    public Material blueMaterial;
    public Material purpleMaterial;


    void Start()
    {
        InvokeRepeating("Generate", 3, 2);
    }

    void Generate()
    {
        int x = (int)Random.Range(borderLeft.position.x, borderRight.position.x);
        int z = (int)Random.Range(borderBottom.position.z,borderTop.position.z);
        Instantiate(foodPrefab, new Vector3(x, 0.0f, z),Quaternion.identity);
        int color = (int)Random.Range(1, 5);
        switch (color)
        {
            case 1:
                foodPrefab.GetComponent<Renderer>().material = redMaterial;
                break;
            case 2:
                foodPrefab.GetComponent<Renderer>().material = greenMaterial;
                break;
            case 3:
                foodPrefab.GetComponent<Renderer>().material = yellowMaterial;
                break;
            case 4:
                foodPrefab.GetComponent<Renderer>().material = blueMaterial;
                break;
            case 5:
                foodPrefab.GetComponent<Renderer>().material = purpleMaterial;
                break;
        }
    }

}
